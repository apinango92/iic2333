1. El programa funciona bien para una cola, pero no para N colas.
2. Se modelaron todas las colas con una cola y un campo para el thread que indica el número de cola que le corresponde.
3. 'thead_unblock' inserta en la cola única el thread 't'. Para facilitar la búsqueda posterior en esta cola, se adoptó un orden parcial, donde los threads están ordenados por prioridad dentro de cada cola, pero no están ordenados por prioridad entre distintas colas (a lo heap). Se inserta inmediatamente antes de cualquier que sea más chico que él y esté en la misma cola (de acuerdo al campo de thread que se agregó, que indica el número de cola), partiendo la búsqueda desde el principio de la cola.
4. 'next_thread_to_run_nq' retorna el siguiente thread a ejecutar. Busca el de ayor prioridad en la cola en ejecución y en caso de no haber ninguno, busca el de mayor prioridad de la siguiente cola (siguiendo la polìtica round robin). Si llega a la última cola, se devuelve.
5. 'init_thread' asigna el thread recién inicializado a la cola 1 y le setea todos los campos para los campos estadísticos a 0.

